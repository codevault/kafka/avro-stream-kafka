package com.learning.kafka.streams;

import com.learning.kafka.KeyData;
import com.learning.kafka.ValueData;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;

public class AvroStream {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvroStream.class);
    private static final Supplier<String> SCHEMA_REGISTRY_URL = () -> "http://localhost:8081";
    private static final Supplier<Serde<KeyData>> KEY_DATA_SERDE = () -> {
        Map<String, String> serdeConfig = Collections.singletonMap(
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, SCHEMA_REGISTRY_URL.get());
        Serde<KeyData> keySpecificAvroSerde = new SpecificAvroSerde<>();
        keySpecificAvroSerde.configure(serdeConfig, true);
        return keySpecificAvroSerde;
    };
    private static final Supplier<Serde<ValueData>> VALUE_DATA_SERDE = () -> {
        Map<String, String> serdeConfig = Collections.singletonMap(
                AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, SCHEMA_REGISTRY_URL.get());
        Serde<ValueData> valueSpecificAvroSerde = new SpecificAvroSerde<>();
        valueSpecificAvroSerde.configure(serdeConfig, false);
        return valueSpecificAvroSerde;
    };

    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "avro-app");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, SpecificAvroSerde.class);
        properties.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, SCHEMA_REGISTRY_URL.get());

        StreamsBuilder builder = new StreamsBuilder();
        builder.stream("avro_input", Consumed.with(KEY_DATA_SERDE.get(), VALUE_DATA_SERDE.get()))
                .peek((k, v) -> LOGGER.info("Key: {}, Value: {}", k, v))
                .to("avro_output", Produced.with(KEY_DATA_SERDE.get(), VALUE_DATA_SERDE.get()));

        KafkaStreams streams = new KafkaStreams(builder.build(), properties);

        LOGGER.info("Stating Kafka Streams...");
        streams.start();
    }
}
